import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { CardModule } from './components/card/card.module';
import { StoreModule } from '@ngrx/store';
import { cardReducer } from './components/card/card.reducer';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CardModule,
    StoreModule.forRoot({ stateCard: cardReducer })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
