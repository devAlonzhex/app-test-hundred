import { createAction } from '@ngrx/store';

export const add = createAction('ADD-CARD');
export const remove = createAction('REMOVE-CARD');
export const update = createAction('UPDATE-CARD');
