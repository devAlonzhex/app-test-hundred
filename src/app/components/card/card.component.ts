import { Component } from "@angular/core";
import { ICard, IStateCard } from './commons/model/card.model';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})

export class CardComponent {
  state: Observable<IStateCard>;
  viewFormUpdate: boolean;
  itemSelected: ICard;

  constructor(private store: Store<{ stateCard: IStateCard }>) {
    this.state = store.pipe(select('stateCard'));
    this.viewFormUpdate = false;
    this.itemSelected = {} as ICard;
  }

  ngOnInit() {
  }

  viewEdit(data: ICard) {
    this.itemSelected = data;
    this.viewFormUpdate = true;
  }

  close() {
    this.viewFormUpdate = false;
  }
}