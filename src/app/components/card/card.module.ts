import { NgModule } from "@angular/core";
import { CardComponent } from './card.component';
import { BrowserModule } from '@angular/platform-browser';
import { ItemCardComponent } from './commons/components/item/item.component';
import { UpdateCardComponent } from './commons/components/update/update.component';
import { ComponentsCardModule } from './commons/components/component.module';

@NgModule({
  declarations: [
    CardComponent,
    // ItemCardComponent,
    // UpdateCardComponent
  ],
  imports: [
    BrowserModule,
    ComponentsCardModule
  ],
  exports: [
    CardComponent,
    // ItemCardComponent,
    // UpdateCardComponent
  ]
})

export class CardModule { }