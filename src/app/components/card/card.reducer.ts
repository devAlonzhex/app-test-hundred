import { createReducer, on } from '@ngrx/store';
import { add, remove, update } from './card.actions';
import { IStateCard, ICard } from './commons/model/card.model';
import { listCard } from './card.constants';

export const initialState: IStateCard = {
  list: listCard,
  card: {} as ICard
};

const _cardReducer = createReducer(initialState,
  on(add, state => addCard(state)),
  on(update, state => updateCard(state)),
  on(remove, state => deleteCard(state)),
);

export function cardReducer(state: IStateCard, action: any) {
  return _cardReducer(state, action);
}

function addCard(val: IStateCard) {
  const id = Math.floor(Math.random() * 60000) + 1;
  const card = { id: id, titulo: 'card new', comentario: 'nuevo card por defecto', imagen: 'default.png' };
  val.list.push(card);
  return val;
}

function deleteCard(val: IStateCard) {
  let list = val.list.filter(x => x !== val.card);
  val.list = list;
  return val; // Object.assign({}, { list });
}

function updateCard(val: IStateCard) {
  let list = [...val.list];
  const index = list.findIndex(x => x.id === val.card.id);
  list[index] = val.card;
  val.list = list;
  return val;
}

