import { ICard } from './commons/model/card.model';

export const listCard: ICard[] = [
  { id: 1, titulo: 'card 1', comentario: 'primer card', imagen: 'default.png' },
  { id: 2, titulo: 'card 2', comentario: 'segundo card', imagen: 'default.png' }
];
