export interface ICard {
  id: number;
  titulo: string;
  comentario: string;
  imagen: string;
}

export interface IStateCard {
  card: ICard;
  list: ICard[];
}