import { Component, Input, Output, EventEmitter } from "@angular/core";
import { ICard, IStateCard } from '../../model/card.model';
import { Store, select } from '@ngrx/store';
import { remove } from '../../../card.actions';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-card-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})

export class ItemCardComponent {
  @Input() card: ICard;
  @Output() cardSelect = new EventEmitter();
  state: Observable<IStateCard>;
  rutaImg: string;

  constructor(private store: Store<{ stateCard: IStateCard }>) {
    this.state = store.pipe(select('stateCard'));
  }

  ngOnInit(){
    this.rutaImg = `./assets/${this.card.imagen}`;
  }

  edit() {
    this.state.subscribe(x => {
      x.card = this.card
    })
    this.cardSelect.emit(this.card);
  }

  delete() {
    this.state.subscribe(x => {
      x.card = this.card
    })
    this.store.dispatch(remove());
  }
}