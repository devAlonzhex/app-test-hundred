import { Component, Input, OnChanges, Output, EventEmitter } from "@angular/core";
import { ICard, IStateCard } from '../../model/card.model';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { update } from '../../../card.actions';

@Component({
  selector: 'app-card-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.scss']
})

export class UpdateCardComponent implements OnChanges {

  @Input() card: ICard;
  @Output() cancel = new EventEmitter();
  state: Observable<IStateCard>;
  formData: FormGroup;

  constructor(
    private store: Store<{ stateCard: IStateCard }>,
    private fb: FormBuilder) {
    this.state = store.pipe(select('stateCard'));
    this.formData = this.fb.group({
      titulo: ['', Validators.required],
      comentario: ['', Validators.required],
    })
  }

  ngOnChanges() {
    this.loadCard();
  }

  ngOnInit() {
    this.loadCard();
  }

  loadCard() {
    this.formData.controls.titulo.setValue(this.card.titulo);
    this.formData.controls.comentario.setValue(this.card.comentario);
  }

  saveData() {
    if (this.formData.valid) {
      this.state.subscribe(x => {
        x.card = {
          id: this.card.id,
          titulo: this.formData.controls.titulo.value,
          comentario: this.formData.controls.comentario.value,
          imagen: 'default.png'
        }
      })
      this.store.dispatch(update());
      this.cancel.emit();
    }
  }
  close() {
    this.cancel.emit();
  }
}