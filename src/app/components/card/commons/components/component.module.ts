import { NgModule } from "@angular/core";
import { ItemCardComponent } from './item/item.component';
import { UpdateCardComponent } from './update/update.component';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

const components = [
  UpdateCardComponent,
  ItemCardComponent,
];

@NgModule({
  declarations: [components],
  exports: [components],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})

export class ComponentsCardModule { }