import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { add } from '../components/card/card.actions';
import { IStateCard } from '../components/card/commons/model/card.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {

  constructor(private store: Store<{ stateCard: IStateCard }>) { }

  add() {
    this.store.dispatch(add());
  }
}
