# app-test-hundred

### Pre requisistos instalados (App Angular 7)
* NodeJS >= 10.12.0
* Npm >= 6.9
* angular-cli@latest


### Instalar la aplicacion
npm install


### En caso de tener problemas en la instalación volver a instalar la libreria ngrx, caso contrario obviar y ejecutar la aplicacion
npm install @ngrx/store --save

### Ejecutar la aplicacion
ng serve

### En caso de que no se abra automaticamente, navegar en la ruta
http://localhost:4200/

